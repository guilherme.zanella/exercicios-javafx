package fxml;

import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginControlador {
	@FXML
	private TextField campoEmail;
	@FXML
	private PasswordField campoSenha;
	
	public void entrar() {
		boolean emailValido = campoEmail.getText().equals("guizanella@email.com");
		boolean senhaValida = campoSenha.getText().equals("gui123");
		
		if(emailValido && senhaValida) {
			System.out.println("sucesso");
		} else {
			System.out.println("falha");
		}
	}
}
