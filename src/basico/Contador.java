package basico;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Contador extends Application {
	private int contador = 0;
	
	private void atualizarLabelNum(Label num) {
		num.setText(Integer.toString(contador));
		
		num.getStyleClass().remove("verde");
		num.getStyleClass().remove("vermelho");
		
		if(contador > 0) {
			num.getStyleClass().add("verde");
		} else if(contador < 0) {
			num.getStyleClass().add("vermelho");
		}
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Label labelTitulo = new Label("Contador");
		labelTitulo.getStyleClass().add("titulo");
		
		Label labelNumero = new Label("0");
		labelNumero.getStyleClass().add("numero");
		
		Button botaoMenos = new Button("-");
		botaoMenos.getStyleClass().add("botoes");
		botaoMenos.setOnAction(e -> {
			contador--;
			atualizarLabelNum(labelNumero);
		});
		
		Button botaoMais = new Button("+");
		botaoMais.getStyleClass().add("botoes");
		botaoMais.setOnAction(e -> {
			contador++;
			atualizarLabelNum(labelNumero);
		});
		
		HBox boxBotoes = new HBox();
		boxBotoes.setAlignment(Pos.CENTER);
		boxBotoes.setSpacing(10);
		boxBotoes.getChildren().add(botaoMenos);
		boxBotoes.getChildren().add(botaoMais);
		
		VBox boxConteudo = new VBox();
		boxConteudo.getStyleClass().add("conteudo");
		boxConteudo.setSpacing(10);
		boxConteudo.setAlignment(Pos.CENTER);
		boxConteudo.getChildren().add(labelTitulo);
		boxConteudo.getChildren().add(labelNumero);
		boxConteudo.getChildren().add(boxBotoes);
		
		String css = getClass().getResource("/basico/Contador.css").toExternalForm();
		Scene cenaPrincipal = new Scene(boxConteudo, 400, 400);
		cenaPrincipal.getStylesheets().add(css);
		cenaPrincipal.getStylesheets().add("https://fonts.googleapis.com/css?family=Oswald");
		
		primaryStage.setScene(cenaPrincipal);
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
